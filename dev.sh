#!/bin/sh

APP_NAME="dev:sda-detect-lang"
PATH_IN_LOCAL_ENV=/home/ahaas/workspace/sda-detect-lang

docker build -t $APP_NAME . --file $PATH_IN_LOCAL_ENV/dev.Dockerfile
docker run -it -P -v $PATH_IN_LOCAL_ENV:/app $APP_NAME