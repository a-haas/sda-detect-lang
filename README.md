# SDA Detect Lang

## Getting started

To run in dev mode
```sh
./dev.sh
make
./build/a.out
```

To run in prod mode
```sh
./prod.sh
```

In both mode it will generate a DAWG (using the dict inside dict folder) and you will be able to detect the lang of the sentence on the input.

To use the trie structure, uncomment the trie part in `main.c` and rebuild the project.

## Build the dictionnary
To rebuild the dictionary run the `./fetch_dict.sh` but it might break. You can change the source if needed.

Anyway the current dict are in the repo and the app will work.

## Project subject
The project subject is available as a *.pdf* at the root of the repo and the Latex sources are available in the subject folder.