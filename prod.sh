#!/bin/sh

APP_NAME="prod:sda-detect-lang"
PATH_IN_LOCAL_ENV=/home/ahaas/workspace/sda-detect-lang

docker build -t $APP_NAME . --file $PATH_IN_LOCAL_ENV/prod.Dockerfile
docker run -it -P $APP_NAME