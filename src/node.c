#include <stdio.h>
#include <stdlib.h>
#include "node.h"

Node new_node() {
    static size_t id = 1;
    
    Node n = (Node) malloc(sizeof(struct node));
    n->id = id++;
    n->final = false;
    for(size_t i = 0; i < 26; i++) {
        n->children[i] = NULL;
    }
    
    return n;
}

void free_node(Node node) {
    if(node == NULL) {
        return;
    }

    free(node);
}

void delete_node(Node node) {
    if(node == NULL) {
        return;
    }
    
    for(size_t i = 0; i < 26; i++) {
        delete_node(node->children[i]);
    }
    free(node);
}

size_t node_dfs(Node node) {
    if(node == NULL) {
        return 0;
    }

    size_t count = 1;
    for(unsigned i = 0; i < 26; i++) {
        count += node_dfs(node->children[i]);
    }

    return count;
}