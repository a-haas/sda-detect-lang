#ifndef __TRIE_H
#define __TRIE_H

#include "node.h"

void insert_in_trie(Node root, char *word);
bool is_in_trie(Node root, char *word);

#endif