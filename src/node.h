#ifndef __NODE_H
#define __NODE_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
    size_t id;
    struct node *children[26];
    bool final;
} *Node;

Node new_node();
void free_node(Node node);
void delete_node(Node node);
size_t node_dfs(Node node);

#endif