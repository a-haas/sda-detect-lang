#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "trie.h"
#include "dawg.h"
#include "utils.h"
#include "node.h"

void assert_trie(Node trie, char *dict) {
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE *fp;
    
    // read file 
    fp = fopen(dict, "r"); // read mode
    if (fp == NULL) {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        // remove newline
        size_t length = strlen(line);
        if((length > 0) && (line[length-1] == '\n'))
        {
            line[length-1] ='\0';
        }
        // generate trie
        assert(is_in_trie(trie, line));
    }

    fclose(fp);
    free(line);
}

Node generate_trie(char *dict) {
    Node trie = new_node();

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE *fp;
    
    // read file 
    fp = fopen(dict, "r"); // read mode
    if (fp == NULL) {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        // remove newline
        size_t length = strlen(line);
        if((length > 0) && (line[length-1] == '\n'))
        {
            line[length-1] ='\0';
        }
        parse_word(line);
        // generate trie
        insert_in_trie(trie, line);
    }

    fclose(fp);
    free(line);

    assert_trie(trie, dict);

    return trie;
}

void assert_dawg(Dawg dawg, char *dict) {
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE *fp;
    
    // read file 
    fp = fopen(dict, "r"); // read mode
    if (fp == NULL) {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        // remove newline
        size_t length = strlen(line);
        if((length > 0) && (line[length-1] == '\n'))
        {
            line[length-1] ='\0';
        }
        // generate trie
        assert(is_in_dawg(dawg, line));
    }

    fclose(fp);
    free(line);
}

Dawg generate_dawg(char *dict) {
    Dawg dawg = new_dawg();

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE *fp;
    
    // read file 
    fp = fopen(dict, "r"); // read mode
    if (fp == NULL) {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        // remove newline
        size_t length = strlen(line);
        if((length > 0) && (line[length-1] == '\n'))
        {
            line[length-1] ='\0';
        }
        // generate trie
        insert_in_dawg(dawg, line);
    }
    finish_dawg(dawg);

    fclose(fp);
    free(line);

    assert_dawg(dawg, dict);

    return dawg;
}

// Nodes in trie fr = 623994
// Nodes in trie de = 1229117
// Nodes in trie en = 606879
// Nodes in dawg fr = 34202
// Nodes in dawg de = 146205
// Nodes in dawg en = 80075

int main(int argc, char* argv[]) {
    // Node french_trie = generate_trie("dict/french-wordlist.txt");
    // Node english_trie = generate_trie("dict/english-wordlist.txt");
    // Node german_trie = generate_trie("dict/german-wordlist.txt");

    // printf("Nombre de noeud trie fr = %lu\n", node_dfs(french_trie));
    // printf("Nombre de noeud trie de = %lu\n", node_dfs(german_trie));
    // printf("Nombre de noeud trie en = %lu\n", node_dfs(english_trie));

    // delete_node(french_trie);
    // delete_node(english_trie);
    // delete_node(german_trie);

    Dawg french_dawg = generate_dawg("dict/french-wordlist.txt");
    Dawg english_dawg = generate_dawg("dict/english-wordlist.txt");
    Dawg german_dawg = generate_dawg("dict/german-wordlist.txt");

    printf("Nombre de noeud dawg fr = %lu\n", node_dfs(french_dawg->root));
    printf("Nombre de noeud dawg de = %lu\n", node_dfs(german_dawg->root));
    printf("Nombre de noeud dawg en = %lu\n", node_dfs(english_dawg->root));

    char str[1024];
    printf("Please enter your text: ");
    scanf("%[^\n]", str);

    char *token = strtok(str, " ");
    size_t french_count = 0;
    size_t english_count = 0;
    size_t german_count = 0;

	while (token != NULL)
	{
        if(is_in_dawg(german_dawg, token)) {
            german_count += 1;
        }
        if(is_in_dawg(french_dawg, token)) {
            french_count += 1;
        }
        if(is_in_dawg(english_dawg, token)) {
            english_count += 1;
        }

		token = strtok(NULL, " ");
	}

    if(german_count > french_count && german_count > english_count) {
        printf("Your text is more likely to be in german\n");        
    }
    else if(french_count > english_count) {
        printf("Your text is more likely to be in french\n");
    }
    else {
        printf("Your text is more likely to be in english\n");
    }

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE *fp;
    
    // read file 
    fp = fopen("dict/english-wordlist.txt", "r"); // read mode
    if (fp == NULL) {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        // remove newline
        size_t length = strlen(line);
        if((length > 0) && (line[length-1] == '\n'))
        {
            line[length-1] ='\0';
        }
        // generate trie
        assert(is_in_dawg(english_dawg, line));
    }

    fclose(fp);
    free(line);
    
    delete_dawg(french_dawg);
    delete_dawg(english_dawg);
    delete_dawg(german_dawg);
    
    return 0;
}