#include <string.h>
#include <stdbool.h>

#include "trie.h"
#include "utils.h"

void insert_in_trie(Node root, char *word) {
    Node current_node = root;
    for(size_t i = 0; i < strlen(word); i++) {
        size_t char_index = ascii_to_index(word[i]);
        
        // if child node for given char do not exist, create it
        if(current_node->children[char_index] == NULL) {
            current_node->children[char_index] = new_node();
        }

        current_node = current_node->children[char_index];
    }

    // at the end of the word, final is true
    current_node->final = true;
}

bool is_in_trie(Node root, char *word) {
    Node current_node = root;
    
    for(size_t i = 0; i < strlen(word); i++) {
        size_t char_index = ascii_to_index(word[i]);
        
        // if child node for given char do not exist, the word isn't in the dict
        if(current_node->children[char_index] == NULL) {
            return false;
        }

        current_node = current_node->children[char_index];
    }

    // if last node is not a final node, the word isn't in the dict
    return current_node->final;
}