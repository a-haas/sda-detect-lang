#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "stack.h"
#include "dawg.h"
#include "utils.h"
#include "hashmap.h"

Dawg new_dawg() {
    Dawg dawg = (Dawg) malloc(sizeof(struct dawg_s));
    dawg->root = new_node();
    dawg->previous_word = malloc(sizeof(char) * 1);
    strcpy(dawg->previous_word, "");
    dawg->unregistered = new_stack(1024);
    dawg->registered = (Hashmap) malloc(sizeof(struct hashmap_s));
    hashmap_create(1024, dawg->registered);

    return dawg;
}

char *_node_as_string(Node node) {
    char *old_str;
    char *str = concat("", node->final ? "1_" : "0_");
    for(size_t i=0; i < 26; i++) {
        if(node->children[i] != NULL) {
            char edge_as_str[32];
            sprintf(edge_as_str, "%c-%lu_", (char) (i+97), (node->children[i])->id);
            old_str = str;
            str = concat(str, edge_as_str);
            free(old_str);
        }
    }

    return str;
}

void _minimize(Dawg dawg, size_t depth) {
    for(size_t i = stack_size(dawg->unregistered); i > depth ; i--) {
        Edge last_unchecked = stack_pop(dawg->unregistered);
        char *node_as_str = _node_as_string(last_unchecked->to);
        Node in_hashmap = hashmap_get(dawg->registered, node_as_str, strlen(node_as_str));
        if(in_hashmap != NULL) {
            last_unchecked->from->children[ascii_to_index(last_unchecked->label)] = in_hashmap;
            free_node(last_unchecked->to);
            free(node_as_str);
        }
        else {
            hashmap_put(dawg->registered, node_as_str, strlen(node_as_str), last_unchecked->to);
        }
        free(last_unchecked);
    }
}

void insert_in_dawg(Dawg dawg, char *word) {
    size_t common_prefix = 0;
    for(size_t i = 0; i < strlen(dawg->previous_word) && i < strlen(word); i++) {
        if(word[i] != dawg->previous_word[i]) {
            break;
        }
        common_prefix += 1;
    }

    _minimize(dawg, common_prefix);

    Node node;
    if(is_stack_empty(dawg->unregistered)) {
        node = dawg->root;
    }
    else {
        node = ((Edge) stack_peek(dawg->unregistered))->to;
    }

    for(size_t i = common_prefix; i < strlen(word); i++) {
        Node next = new_node();
        node->children[ascii_to_index(word[i])] = next;
        Edge edge = malloc(sizeof(struct edge_s));
        edge->from = node;
        edge->label = word[i];
        edge->to = next;
        stack_push(dawg->unregistered, edge);
        node = next;
    }

    node->final = true;
    dawg->previous_word = realloc(dawg->previous_word, strlen(word) + 1);
    strcpy(dawg->previous_word, word);
}
 
void finish_dawg(Dawg dawg){
    _minimize(dawg, 0);

    // free what has be done to complete the DAWG
    free(dawg->previous_word);
    free_stack(dawg->unregistered);
}

bool is_in_dawg(Dawg dawg, char *word) {
    Node current_node = dawg->root;
    
    for(size_t i = 0; i < strlen(word); i++) {
        size_t char_index = ascii_to_index(word[i]);
        
        // if child node for given char do not exist, the word isn't in the dict
        if(current_node->children[char_index] == NULL) {
            return false;
        }

        current_node = current_node->children[char_index];
    }

    // if last node is not a final node, the word isn't in the dict
    return current_node->final;
}

int free_all(void* const context, struct hashmap_element_s* const e) {
    free_node((Node) e->data);
    free((char *)e->key);
    return -1;
}

void delete_dawg(Dawg dawg) {
    void *const context;
    hashmap_iterate_pairs(dawg->registered, free_all, (void *)context);
    // free hashmap
    hashmap_destroy(dawg->registered);
    free(dawg->registered);
    free_node(dawg->root);
    free(dawg);
}