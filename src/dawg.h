#ifndef __DAWG_H
#define __DAWG_H

#include <stdbool.h>
#include "node.h"
#include "hashmap.h"
#include "stack.h"

typedef struct hashmap_s *Hashmap;

typedef struct edge_s {
    Node from;
    char label;
    Node to;
} *Edge;


typedef struct dawg_s {
    char *previous_word;
    Stack unregistered;
    Hashmap registered;
    Node root;
} *Dawg;

Dawg new_dawg();
void finish_dawg(Dawg dawg);
void insert_in_dawg(Dawg dawg, char *word);
bool is_in_dawg(Dawg dawg, char *word);
void delete_dawg(Dawg dawg);

#endif