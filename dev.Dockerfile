# GCC support can be specified at major, minor, or micro version
# (e.g. 8, 8.2 or 8.2.0).
# See https://hub.docker.com/r/library/gcc/ for all supported GCC
# tags from Docker Hub.
# See https://docs.docker.com/samples/library/gcc/ for more on how to use this image
FROM gcc:latest

# Update apps on the base image
RUN apt-get -y update && apt-get install -y

# Install dev niceties (ZSH)
RUN apt-get -y install zsh
# Change default shell to ZSH
RUN chsh -s $(which zsh)
# Install Oh My Zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# ZSH config
RUN rm ~/.zshrc
COPY dotfiles /root/

# Install debug apps
RUN apt-get -y install valgrind gdb

WORKDIR /app

CMD ["/usr/bin/zsh"]